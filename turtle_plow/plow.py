#!/usr/bin/env python3

import rclpy
from rclpy.node import Node
from turtlesim.msg import Pose
from geometry_msgs.msg import Twist
from turtlesim.srv import SetPen
from turtlesim.srv import Kill
from turtlesim.srv import Spawn
from functools import partial
from math import pi

# 定义耕地节点类
class Plow(Node):

    def __init__(self):
        super().__init__("turtle_plow")
        self.first_swath = True
        self.call_kill_service('turtle1')
        self.call_spawn_service(0.5, 0.1, pi/2, 'turtle2')
        self.publisher_ = self.create_publisher(Twist, "/turtle2/cmd_vel", 10)
        self.subscriber_ = self.create_subscription(Pose, "/turtle2/pose", self.pose_callback, 10)
        self.get_logger().info("耕地节点启动成功")

    # 订阅pose的回调函数，即一旦收到消息后的处理函数
    def pose_callback(self, pose: Pose):
        cmd = Twist()
        if pose.x > 1.0:
            self.first_swath = False
        if self.first_swath:
            if pose.y < 10.5:
                cmd.linear.x = 3.0
                cmd.angular.z = 0.0
            else:
                cmd.linear.x = 1.0
                cmd.angular.z = -pi
        else:
            if pose.y > 0.5 and pose.y < 10.5:
                cmd.linear.x = 3.0
                cmd.angular.z = 0.0
                if pose.theta > 0: # 上行
                    if pose.theta != pi/2:
                        delta = pi/2 - pose.theta
                        cmd.angular.z = delta * 10.0
                else: # 下行
                    if pose.theta != -pi/2:
                        delta = pi/2 + pose.theta
                        cmd.angular.z = -delta * 10.0
            elif pose.y <= 0.5:
                cmd.linear.x = 1.0
                cmd.angular.z = pi
            else:
                cmd.linear.x = 1.0
                cmd.angular.z = -pi
        if pose.x >= 10.5 and pose.y <= 0.5:
            cmd.linear.x = 0.0
            cmd.angular.z = 0.0
        
        self.get_logger().info("𝚹:" + str(pose.theta) + "x:" + str(pose.x) + "y:" + str(pose.y))
        
        self.publisher_.publish(cmd)

    # 调用设置轨迹属性服务的函数
    def call_set_pen_service(self, r, g, b, width, off):
        client = self.create_client(SetPen, "/turtle2/set_pen")
        while not client.wait_for_service(1.0):
            self.get_logger().warn("Waiting for set pen service...")
        
        request = SetPen.Request()
        request.r = r
        request.g = g
        request.b =b
        request.width = width
        request.off = off

        future = client.call_async(request)
        future.add_done_callback(partial(self.callback_set_pen))

    def callback_set_pen(self, future):
        try:
            response = future.result
        except Exception as e:
            self.get_logger().error("Service call failed: %r" % (e,))
    
    # 调用删除乌龟服务的函数
    def call_kill_service(self, name):
        client = self.create_client(Kill, "/kill")
        while not client.wait_for_service(1.0):
            self.get_logger().warn("Waiting for kill turtle service...")
        
        request = Kill.Request()
        request.name = name

        future = client.call_async(request)
        future.add_done_callback(partial(self.callback_kill))

    def callback_kill(self, future):
        try:
            response = future.result
        except Exception as e:
            self.get_logger().error("Service call failed: %r" % (e,))
    
    # 调用创建乌龟服务的函数
    def call_spawn_service(self, x, y, theta, name):
        client = self.create_client(Spawn, "/spawn")
        while not client.wait_for_service(1.0):
            self.get_logger().warn("Waiting for spawn turtle service...")
        
        request = Spawn.Request()
        request.x = x
        request.y = y
        request.theta = theta
        request.name = name

        future = client.call_async(request)
        future.add_done_callback(partial(self.callback_spawn))

    def callback_spawn(self, future):
        try:
            response = future.result
        except Exception as e:
            self.get_logger().error("Service call failed: %r" % (e,))

# 定义主运行函数
def run(args=None):
    rclpy.init(args=args)
    node = Plow()
    rclpy.spin(node)
    rclpy.shutdown()
