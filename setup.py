from setuptools import find_packages, setup

package_name = 'turtle_plow'

setup(
    name=package_name,
    version='0.0.0',
    packages=find_packages(exclude=['test']),
    data_files=[
        ('share/ament_index/resource_index/packages',
            ['resource/' + package_name]),
        ('share/' + package_name, ['package.xml']),
    ],
    install_requires=['setuptools'],
    zip_safe=True,
    maintainer='zth',
    maintainer_email='zth@xjau.edu.cn',
    description='乌龟耕地',
    license='通用公共许可协议',
    tests_require=['pytest'],
    entry_points={
        'console_scripts': [
            "plow = turtle_plow.plow:run"
        ],
    },
)
